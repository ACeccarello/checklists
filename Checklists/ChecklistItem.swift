//
//  ChecklistItem.swift
//  Checklists
//
//  Created by Alessandro Ceccarello on 6/9/15.
//  Copyright © 2015 La Fuente Digital. All rights reserved.
//

import Foundation


class ChecklistItem {
    var text = ""
    var checked = false
    
    func toggleChecked() {
        checked = !checked
    }
}